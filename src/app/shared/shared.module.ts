import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const imports: any[] = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
];

@NgModule({
  declarations: [
    PrivacyPolicyComponent
  ],
  imports: [
    ...imports,
  ],
  exports: [
    ...imports,
  ]
})
export class SharedModule { }
