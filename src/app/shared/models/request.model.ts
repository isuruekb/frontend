export class RequestModel {
    
    public id?: number;
    public title?: string;
    public status?: string;
    public date?: string;
    public time?: string;
    public message?: string;
    public imageID?: string;
    // public user: User;

    constructor(
        title?: string,
        message?: string
        ) {
        if (title) {
            this.title = title;
            this.message = message;
            
        }
    }
}
