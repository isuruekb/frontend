import { UserType } from "../types/user.types";
import { BaseModel } from "./base.model";

export class UserModel extends BaseModel {
    public email: string = '';

    public fName?: string = '';

    public lName?: string = '';

    public password: string = '';

    public org?: string = '';

    public contact?: string = '';

    public address?: string = '';

    public userType: UserType = 'SEEKER';

    public status: boolean = true;

    constructor() {
        super();
        this.email = '';
        this.fName = '';
        this.lName = '';
        this.password = '';
        this.org = '';
        this.contact = '';
        this.address = '';
        // this.userType = '';
        // this.status = '';

    }

    
}
