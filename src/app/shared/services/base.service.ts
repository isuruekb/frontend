import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseService {

  protected http?: HttpClient;
  constructor(injector: Injector) {
    this.http = injector.get(HttpClient);
  }

  protected headers(): any {
    const auth = localStorage.getItem(environment.token_key);
    if (auth) {
      return {
        headers: new HttpHeaders({
          Authorization: 'Bearer ' + auth,
          'Content-Type': 'application/json',
        }),
        params: {},
      };
    }
  }
}
