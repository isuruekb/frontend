import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RequestModel } from '../models/request.model';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  readonly url: string = `${environment.endpoint}request/`;
  constructor(private http: HttpClient) { }

  public find(id: number): Observable<RequestModel> {
    return this.http.get<RequestModel>(`${this.url}find/${id}`);
  }

  public query(): Observable<RequestModel[]> {
    return this.http.post<RequestModel[]>(`${this.url}query`, {});
  }

  public create(model: RequestModel): Observable<any> {
    return this.http.post<any>(`${this.url}create`, model);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.url}delete/${id}`);
  }

}
