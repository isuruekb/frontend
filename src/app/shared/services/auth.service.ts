import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginModel } from '../models/login.model';
import { LogoutModel } from '../models/logout.model';
import { UserModel } from '../models/user.model';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends BaseService {
  [x: string]: any;

  public user: BehaviorSubject<UserModel | null> = new BehaviorSubject<UserModel | null>(null);
  public isAuthenticated: Observable<UserModel | null> = this.user.asObservable();

  constructor(injector: Injector) {
    super(injector);
  }



  public identify(): any {
    return this.http?.get(`${environment.endpoint}auth/whoIsThis`, this.headers());
  }

  public signUp(model: UserModel) {
    return this.http?.post<any>(environment.endpoint + 'user/register', model);
  }

  public signIn(model: LoginModel) {
    return this.http?.post<any>(environment.endpoint + 'auth/login', model);
  }

  public logOut(model: LogoutModel) {
    return this.http?.post<any>(environment.endpoint + 'auth/logout', model);
  }
}
