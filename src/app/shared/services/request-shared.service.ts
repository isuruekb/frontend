import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { RequestModel } from '../models/request.model';

@Injectable({
  providedIn: 'root'
})
export class RequestSharedService {
  public recordResponse: Subject<RequestModel> = new Subject();
  constructor() { }
}
