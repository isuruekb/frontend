import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginModel } from '../shared/models/login.model';
import { UserModel } from '../shared/models/user.model';
import { AuthService } from '../shared/services/auth.service';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  public signUpModel: UserModel = new UserModel();
  public loginModel: LoginModel = new LoginModel();
  

  public submitted: boolean = false;
  public mode: 'sign-up-mode' | 'login-mode' = 'login-mode';

  constructor(
    private service: AuthService, 
    private router: Router, 
    private authService: SocialAuthService,
    private snackBar: MatSnackBar
  ) { }

  signInWithFacebook(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((user: SocialUser) => {
        this.handleSocialSignIn(user);
      })
      .catch((error) => {
        console.error('Error signing in with Facebook:', error);
      });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((user: SocialUser) => {
        this.handleSocialSignIn(user);
      })
      .catch((error) => {
        console.error('Error signing in with Google:', error);
      });
  }

  private handleSocialSignIn(user: SocialUser): void {
    // Assuming your backend API takes a social user token and returns a JWT token
    this.service.socialSignIn(user.authToken)?.subscribe((response: { token: string }) => {
      localStorage.setItem(environment.token_key, response.token);
      this.service.identify()?.subscribe((user: UserModel) => {
        this.service.user.next(user);
        this.router.navigate(['dashboard']);
        // if (user.userType === 'DONATOR') {
        //   this.router.navigate(['dashboard']);
        // } else if (user.userType === 'SEEKER') {
        //   this.router.navigate(['seeker-dashboard']);
        // }
      });
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  public onSingUp(): void {
    this.mode = 'sign-up-mode'; //login ui animation
  }

  public onLogin(): void {
    this.mode = 'login-mode'; 
  }

  // public signIn(): void {
  //   this.service.signIn(this.loginModel)?.subscribe((response: { token: string }) => {
  //     if (response) {
  //       localStorage.setItem(environment.token_key, response.token);
  //       this.service.identify()?.subscribe((user: UserModel) => {
  //         this.service.user.next(user);
  //         if (user.userType === 'DONATOR') {
  //           this.router.navigate(['dashboard']); //exsisting url
            
  //         } else if (user.userType === 'SEEKER') {
  //           this.router.navigate(['seeker-dashboard']);
  //         }
  //       });
  //     } else {
  //       console.log('error', response);
  //     }
  //   })
  // }

  public signIn(): void {
    this.service.signIn(this.loginModel)?.subscribe(
      (response: { token: string }) => {
        if (response) {
          localStorage.setItem(environment.token_key, response.token);
          this.service.identify()?.subscribe((user: UserModel) => {
            this.service.user.next(user);
            if (user.userType === 'DONATOR') {
              this.router.navigate(['dashboard']);
            } else if (user.userType === 'SEEKER') {
              this.router.navigate(['seeker-dashboard']);
            }
          });
        } else {
          console.log('Response was empty or undefined');
        }
      },
      (error) => {
        if (error.error === 'Invalid Credentials') {
          this.snackBar.open('Invalid Credentials', 'Error', {
            duration: 3000, // duration in milliseconds
            horizontalPosition: 'end', // Position horizontally to the right
            verticalPosition: 'top', // Position vertically at the top
          });
        } else {
          console.log('Error occurred:', error);
        }
      }
    );
  }
  
  public signUp(): void {
    this.service.signUp(this.signUpModel)?.subscribe((response: { token: string }) => {

      localStorage.setItem(environment.token_key, response.token);
      if (this.signUpModel.userType === 'DONATOR') {
        this.router.navigate(['dashboard']);
        
      } else if (this.signUpModel.userType === 'SEEKER') {
        this.router.navigate(['seeker-dashboard']);
      }
      

      console.log('submit');
    }, (error) => {
      // message error
    });
  }

  public privacyPolicy(): void{
    this.router.navigate(['/privacy-policy']);
  } 


  
}
