import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { DashComponent } from './dash/dash.component';
import { DashboardComponent } from './dashboard.component';
import { PostsComponent } from './posts/posts.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'posts',
        component: PostsComponent,
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full', 
      },
      {
        path: '**',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
