import { Component, OnInit } from '@angular/core';
import { UserModel } from '../shared/models/user.model';
import { AuthService } from '../shared/services/auth.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

}
