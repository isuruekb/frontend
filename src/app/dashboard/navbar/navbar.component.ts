import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { LogoutModel } from 'src/app/shared/models/logout.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  public logoutModel: LogoutModel = new LogoutModel();

  constructor(private breakpointObserver: BreakpointObserver, private service: AuthService, private router: Router) {}

  public logOut(): void {
    this.service.logOut(this.logoutModel)?.subscribe((response: { token: string }) => {
      console.log('logout');
      localStorage.clear();
      this.router.navigate(['']);
      
    })
  }  

}
