import { Component } from '@angular/core';
import { UserModel } from './shared/models/user.model';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Sponsorship Management System';

  constructor(private authService: AuthService) {
    this.doAuth();
  }

  private doAuth(): void {
    this.authService.identify()?.subscribe((user: UserModel) => {
      // user 
      if (user) {
        this.authService.user.next(user);
      }

    }, (error: any) => {
      // expect 400 - > logout
    });

  }
}
