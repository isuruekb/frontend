import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SeekerDashboardComponent } from './seeker-dashboard.component';
import { AuthGuard } from './auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { DashComponent } from './dash/dash.component';
import { RequestComponent } from './request/request.component';

const routes: Routes = [
  {
    path: '',
    component: SeekerDashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'request',
        component: RequestComponent,
      },
      {
        path: '',
        redirectTo: 'seeker-dashboard',
        pathMatch: 'full', 
      },
      {
        path: '**',
      },
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
    // CommonModule
  ],
  exports: [RouterModule]
})
export class SeekerDashboardRoutingModule { }
