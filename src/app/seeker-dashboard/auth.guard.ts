import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { UserModel } from '../shared/models/user.model';
import { AuthService } from '../shared/services/auth.service';
import { UserType } from '../shared/types/user.types';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  public canActivate(): Observable<boolean> {

    return this.auth.identify()
      .pipe(
        switchMap((auth: UserModel) => {
          this.auth.user.next(auth);
          console.log(auth, 'top');
          const userType: UserType = auth.userType;
          switch (userType) {
            case 'SEEKER':
              return of(true);
            default:
              return of(false);
          }
        }),
        tap((auth) => {
          if (!auth) this.router.navigate(['/']).catch(e => console.warn(e));
        }),
        catchError((error) => {
          this.router.navigate(['/']);
          
          return of(false);
        })
      );
  }

  private doUnauthorizedRedirect(): boolean {
    this.router.navigate(['/']);
    return false;
  }
  
}
