import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeekerDashboardComponent } from './seeker-dashboard.component';
import { SeekerDashboardRoutingModule } from './seeker-dashboard-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { DashComponent } from './dash/dash.component';
import { ProfileComponent } from './profile/profile.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { RequestComponent } from './request/request.component';
import { AddRequestComponent } from './request/add-request/add-request.component';
import { ListRequestComponent } from './request/list-request/list-request.component';



@NgModule({
  declarations: [
    SeekerDashboardComponent,
    NavbarComponent,
    DashComponent,
    ProfileComponent,
    RequestComponent,
    AddRequestComponent,
    ListRequestComponent,
  ],
  imports: [
    CommonModule,
    SeekerDashboardRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule
  ]
})
export class SeekerDashboardModule { }
