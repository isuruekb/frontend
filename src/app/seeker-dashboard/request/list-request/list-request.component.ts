import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { RequestModel } from 'src/app/shared/models/request.model';
import { RequestSharedService } from 'src/app/shared/services/request-shared.service';
import { RequestService } from 'src/app/shared/services/request.service';
import { ListRequestDataSource, ListRequestItem } from './list-request-datasource';

@Component({
  selector: 'app-list-request',
  templateUrl: './list-request.component.html',
  styleUrls: ['./list-request.component.scss']
})
export class ListRequestComponent implements OnInit, AfterViewInit {
  
  public requestModel: RequestModel[] = [];
  
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<ListRequestItem>;
  dataSource: ListRequestDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name'];

  constructor(private sharedService: RequestSharedService, private service: RequestService) {
    this.dataSource = new ListRequestDataSource();
  }

  ngOnInit(): void {
    this.sharedServe();
    this.fetch();
  }

  private fetch(): void {
    this.service.query().subscribe((result: RequestModel[]) => {
      this.requestModel = result;
    });
  }

  public sharedServe() {

      this.sharedService.recordResponse.subscribe(
        (res: RequestModel) => {
          const index = this.requestModel.findIndex((item: RequestModel) => {
            return item.id === res.id;
          });

          if (index !== -1) {
            this.requestModel[index] = res;
          } else {
            this.requestModel.push(res);
          }
        }
      )
  }


  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
